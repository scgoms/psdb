-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: psdb
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `commentables`
--

LOCK TABLES `commentables` WRITE;
/*!40000 ALTER TABLE `commentables` DISABLE KEYS */;
/*!40000 ALTER TABLE `commentables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `game_platform`
--

LOCK TABLES `game_platform` WRITE;
/*!40000 ALTER TABLE `game_platform` DISABLE KEYS */;
INSERT INTO `game_platform` VALUES (1,1),(2,1),(3,1),(4,1),(5,1);
/*!40000 ALTER TABLE `game_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `game_user`
--

LOCK TABLES `game_user` WRITE;
/*!40000 ALTER TABLE `game_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,'NPWR00163_00','Warhawk','Warhawk',NULL,'2017-09-13 13:52:32','2017-09-13 13:52:32'),(2,'NPWR00688_00','Ashes Cricket 2009','Ashes Cricket 2009',NULL,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(3,'NPWR00133_00','Just Cause 2','Just Cause 2',NULL,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(4,'NPWR00462_00','Uncharted 2: Among Thieves','Uncharted 2: Among Thieves',NULL,'2017-09-13 13:52:45','2017-09-13 13:52:45'),(5,'NPWR01993_00','Crysis','Crysis',NULL,'2017-09-13 13:52:53','2017-09-13 13:52:53');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Warhawk','',1,'2017-09-13 13:52:32','2017-09-13 13:52:32'),(2,'Ashes Cricket 2009','',2,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(3,'Just Cause 2','',3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(4,'Uncharted 2: Among Thieves','',4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(5,'DLC','Requires Uncharted: Drake’s Fortune Multiplayer Pack',4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(6,'UNCHARTED 2: Among Thieves Siege Expansion Pack','Requires Uncharted 2: Among Thieves Siege Expansion Pack',4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(7,'Crysis','',5,'2017-09-13 13:52:58','2017-09-13 13:52:58');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `imageables`
--

LOCK TABLES `imageables` WRITE;
/*!40000 ALTER TABLE `imageables` DISABLE KEYS */;
INSERT INTO `imageables` VALUES (1,4,'App\\Models\\PSDB\\Game'),(2,4,'App\\Models\\PSDB\\Game'),(3,4,'App\\Models\\PSDB\\Game'),(4,4,'App\\Models\\PSDB\\Game'),(5,4,'App\\Models\\PSDB\\Game'),(6,5,'App\\Models\\PSDB\\Game'),(7,5,'App\\Models\\PSDB\\Game'),(8,5,'App\\Models\\PSDB\\Game'),(9,5,'App\\Models\\PSDB\\Game'),(10,5,'App\\Models\\PSDB\\Game');
/*!40000 ALTER TABLE `imageables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,1,'2017-09-13 13:52:45','2017-09-13 13:52:45','images/4/images/E7MM9Fw6CMFPLmdrwN16IIACF92q2QXk0MrPEuWj.jpeg'),(2,1,'2017-09-13 13:52:45','2017-09-13 13:52:45','images/4/images/nmCDjmLIo4foCrHWkUEhC3r4m7c2U4ObEfJCVrpE.jpeg'),(3,1,'2017-09-13 13:52:45','2017-09-13 13:52:45','images/4/images/F1PVLhW1fmYDdJrTo0DCCDwtXDgdBz8xeHxFmFLV.jpeg'),(4,1,'2017-09-13 13:52:45','2017-09-13 13:52:45','images/4/images/JsalsZsH3ceEjyyFm62ed810Q5Vk3c61zwfXdAXu.jpeg'),(5,1,'2017-09-13 13:52:47','2017-09-13 13:52:47','images/4/images/kgk6iEGkmUBoEtXx5p81JaMcdcdAJZcpGMP3jPgg.jpeg'),(6,1,'2017-09-13 13:52:55','2017-09-13 13:52:55','images/5/images/DysYKJhjPGN8y1vSSu3PbJ6hfgci176b6ABR3tn0.jpeg'),(7,1,'2017-09-13 13:52:55','2017-09-13 13:52:55','images/5/images/OoMb46RPqnPw0Y2U0P1j54Ei6Xw67GwbcCybWZb7.jpeg'),(8,1,'2017-09-13 13:52:56','2017-09-13 13:52:56','images/5/images/hhzcKh2I2pueqWQeRx2scXeJZdZOmf0QWgceQyjH.jpeg'),(9,1,'2017-09-13 13:52:57','2017-09-13 13:52:57','images/5/images/cgtqpP4lVims6oL72Dl0IJWxtg9fawK8gcuSWrgH.jpeg'),(10,1,'2017-09-13 13:52:58','2017-09-13 13:52:58','images/5/images/i3Oe6aXlYm3AEfeNE8qhtcxA6NhXbopiMo04Uoq5.jpeg');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (64,'2014_10_12_000000_create_users_table',1),(65,'2014_10_12_100000_create_password_resets_table',1),(66,'2016_05_10_130540_create_permission_tables',1),(67,'2017_07_27_172346_create_posts_table',1),(68,'2017_07_27_172556_create_tags_table',1),(69,'2017_07_27_172630_create_links_table',1),(70,'2017_07_27_203108_create_games_table',1),(71,'2017_07_27_203112_create_groups_table',1),(72,'2017_07_27_203116_create_trophies_table',1),(73,'2017_07_27_203248_create_images_table',1),(74,'2017_07_27_203255_create_videos_table',1),(75,'2017_07_28_181327_create_types_table',1),(76,'2017_07_29_222635_add_path_to_images_table',1),(77,'2017_08_01_171944_create_comments_table',1),(78,'2017_08_11_190319_create_platforms_table',1),(79,'2017_08_19_111205_create_reports_table',1),(80,'2017_08_21_194848_fix_reports_table_for_nullable_values',1),(81,'2017_08_21_205832_add_profile_fields_to_user_table',1),(82,'2017_08_21_205843_add_game_user_table',1),(83,'2017_08_21_210148_create_trophy_user_table',1),(84,'2017_08_23_145933_add_profile_fields_to_users_table',1),(85,'2017_08_24_111028_create_jobs_table',2),(86,'2017_08_24_211338_change_image_path_to_src',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permission_roles`
--

LOCK TABLES `permission_roles` WRITE;
/*!40000 ALTER TABLE `permission_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permission_users`
--

LOCK TABLES `permission_users` WRITE;
/*!40000 ALTER TABLE `permission_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `platforms`
--

LOCK TABLES `platforms` WRITE;
/*!40000 ALTER TABLE `platforms` DISABLE KEYS */;
INSERT INTO `platforms` VALUES (1,'Playstation 3',NULL,'2017-08-24 11:06:18','2017-08-24 11:06:18'),(2,'Playstation 4',NULL,'2017-08-24 11:06:18','2017-08-24 11:06:18'),(3,'Playstation Vita',NULL,'2017-08-24 11:06:18','2017-08-24 11:06:18');
/*!40000 ALTER TABLE `platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role_users`
--

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `trophies`
--

LOCK TABLES `trophies` WRITE;
/*!40000 ALTER TABLE `trophies` DISABLE KEYS */;
INSERT INTO `trophies` VALUES (1,'Warhawk Supreme Achievement','All your base trophies are belong to platinum.',1,1,4.85,1,'2017-09-13 13:52:32','2017-09-13 13:52:32'),(2,'Bandwidth Donor','Host a ranked dedicated server for four consecutive hours.',2,1,21.70,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(3,'Warlord','Create a clan and get nine other members to join. (Awarded at ten members while you are the clan leader.)',4,1,12.55,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(4,'Enlistee','Join a clan.',4,1,44.99,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(5,'Rivalry','Win a clan match (red server) as either clan leader or a regular member.',4,1,10.75,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(6,'Lone Wolf','Play a perfect Deathmatch (top player, no deaths).',4,1,17.45,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(7,'Cowboy','Play a perfect Team Deathmatch (top player, no deaths).',4,1,19.96,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(8,'Hat Trick','Get three flag captures in a single round of CTF.',4,1,37.50,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(9,'World Victory','Control all capturable bases in a Zones match, and reduce the enemy\'s home base to level 1.',4,1,35.27,1,'2017-09-13 13:52:33','2017-09-13 13:52:33'),(10,'Survivalist','Survive for one minute as the hero in Hero mode.',4,1,48.38,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(11,'Collector','Capture four cores at once in Collection mode.',4,1,32.46,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(12,'Anti-Air Ninja','Shoot down a Warhawk with any infantry weapon other than a Rocket Launcher.',4,1,52.90,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(13,'Dead-Eye','Achieve a sniper kill from more than 2,500 feet away.',4,1,33.96,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(14,'Saboteur','Destroy an enemy vehicle with the Field Wrench.',4,1,38.51,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(15,'Irony','Kill an enemy with his own Proximity Mine.',4,1,43.01,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(16,'Invincible','While on foot, take at least 250 damage within 10 seconds without dying.',4,1,63.13,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(17,'Taxi Driver','Help a teammate capture the flag (in CTF) or cores (in Collection) by transporting him back to the base, using any vehicle.',4,1,35.00,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(18,'Resourceful Driver','Achieve a road kill and a .50 cal kill using the same 4x4, without dismounting.',4,1,34.02,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(19,'One In A Million','Shoot down an enemy aircraft using the tank\'s main cannon.',4,1,52.40,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(20,'Overkill','Destroy an enemy aircraft with a volley of 8 Swarm Missiles.',4,1,39.32,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(21,'Dumbfire','Destroy an enemy aircraft with a dumbfired Swarm or Homing Missile.',4,1,36.18,1,'2017-09-13 13:52:34','2017-09-13 13:52:34'),(22,'Remote Pilot','Destroy an enemy aircraft with a TOW Missile.',4,1,34.79,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(23,'How Did You Do That?','Destroy an enemy aircraft with a Cluster Bomb.',4,1,31.71,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(24,'Tail Shaker','Destroy an enemy aircraft with an Aerial Mine within 5 seconds of deploying it.',4,1,40.75,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(25,'Emergency Evasion','Break at least 6 simultaneous missile locks using Chaff.',4,1,25.59,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(26,'Hit List','Kill every member of the opposing team at least once during a round.',4,1,30.94,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(27,'Anti-Camper','Kill an enemy in a Missile or Flak Turret after that enemy has achieved at least five kills in that type of turret.',4,1,34.06,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(28,'Vengeance','Kill an enemy within 60 seconds of that enemy killing you.',4,1,69.46,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(29,'Daredevil','Get 100 feet off the ground in a ground vehicle. Does not count if the vehicle is being carried by a Dropship.',4,1,60.81,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(30,'Ground Pounder','Complete Ground Combat Training.',4,1,72.10,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(31,'Combat Driver','Complete Vehicle Combat Training.',4,1,67.65,1,'2017-09-13 13:52:35','2017-09-13 13:52:35'),(32,'Pilot\'s License','Complete Warhawk Combat Training.',4,1,58.44,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(33,'Bandit Award','Earn all in-game badges at Bandit level.',4,1,14.20,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(34,'Master Award','Earn all in-game badges at Master level.',3,1,5.46,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(35,'Warhawk Award','Earn all in-game badges at Warhawk level.',2,1,4.91,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(36,'Recognition of Merit','Earn any five in-game medals.',4,1,15.95,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(37,'Decorated Soldier','Earn any 15 in-game medals.',3,1,7.22,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(38,'Executive Honor','Earn all in-game medals.',2,1,5.01,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(39,'Chief Sergeant','Achieve a rank of Chief Sergeant.',4,1,28.78,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(40,'Commander','Achieve a rank of Commander.',3,1,7.65,1,'2017-09-13 13:52:36','2017-09-13 13:52:36'),(41,'Lt. Colonel','Achieve a rank of Lt. Colonel.',3,1,5.71,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(42,'General','Achieve a rank of General.',2,1,5.27,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(43,'Minesweeper','Get killed by Proximity Mines five times in a single round.',4,1,41.79,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(44,'Reckless Pilot','Commit suicide in a Warhawk by running into your own Aerial Mine.',4,1,43.79,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(45,'What\'s That Green Line?','Get killed by a Binoculars strike while piloting an aircraft.',4,1,36.53,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(46,'Giant Killer','Shoot down an enemy Dropship while using a Warhawk.',3,1,28.35,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(47,'Flying Fortress','Awarded to the driver and all six passengers of a fully-loaded Dropship.',3,1,13.58,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(48,'Surgical Strike','Shoot down a Dropship with a Binoculars strike.',2,1,24.03,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(49,'King of the Jungle Gym','Retrieve the Proximity Mines from the top of the dome on the Omega Factory Rumble Dome layout.',3,1,20.44,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(50,'Porcupine','Destroy an enemy Warhawk with the APC\'s E-POD shield.',2,1,26.18,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(51,'Behind Enemy Lines','Drive an APC into the enemy\'s home base, then have at least one teammate spawn into that APC.',3,1,27.54,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(52,'Shield Breaker','Break an enemy E-POD shield using the Lightning Shell.',3,1,23.65,1,'2017-09-13 13:52:37','2017-09-13 13:52:37'),(53,'Canyon Run','Using a Warhawk, shoot down an enemy Warhawk in the central gorge in Vaporfield Glacier.',3,1,19.61,1,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(54,'Aerial Ballet','While flying a jetpack, kill an enemy (who is also flying a jetpack) with any weapon other than the Rocket Launcher.',3,1,26.78,1,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(55,'That Was Some Bug','Kill an enemy who is flying a jetpack by hitting him with any vehicle or aircraft.',2,1,25.88,1,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(56,'Are You Aim-Botting?','Snipe an enemy who is flying a jetpack.',3,1,25.81,1,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(57,'Safety Violation','Kill a distant enemy from on top of the salvage crane in Tau Crater.',3,1,21.43,1,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(58,'Best of the Best','Earn all of the Trophies in the game.',1,1,61.67,2,'2017-09-13 13:52:38','2017-09-13 13:52:38'),(59,'Master of the Blade','Successfully complete all of the batting lessons and challenges.',4,1,67.38,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(60,'Master with the Ball','Successfully complete all of the bowling lessons and challenges.',4,1,70.39,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(61,'Dynamite in the Field','Successfully complete all of the fielding lessons.',4,1,78.81,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(62,'Raise the Bat','Score 50 runs in an innings with a single batsman in single player or ranked online match.',4,1,83.48,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(63,'The Big 100','Score 100 runs in an innings with a single batsman in single player or ranked online match.',4,1,78.07,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(64,'The Double Ton','Score 200 runs in an innings with a single batsman in single player or ranked online match.',3,1,71.13,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(65,'The Triple Ton','Score 300 runs in an innings with a single batsman in single player or ranked online match.',3,1,68.43,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(66,'Ashes King with the Bat','Score 500 runs in an Ashes Series with a single batsman in single player.',2,1,66.03,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(67,'Power Hitting','Hit 6 fours in 6 consecutive deliveries in an innings in single player or ranked online match.',4,1,75.25,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(68,'The Blazing Blade','Hit 6 sixes in 6 consecutive deliveries in an innings in single player or ranked online match.',4,1,72.17,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(69,'The Hat Trick','Take 3 wickets in a row from one bowler in a match in single player or ranked online match.',4,1,73.46,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(70,'The Top Hat Trick','Take 4 wickets in a row from one bowler in a match in single player or ranked online match.',3,1,68.24,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(71,'Raise the Ball','Take 5 wickets in an innings with a single bowler in single player or ranked online match.',4,1,77.89,2,'2017-09-13 13:52:39','2017-09-13 13:52:39'),(72,'If You Want Something Done Right','Take 10 wickets in an innings with a single bowler in single player or ranked online match.',2,1,65.60,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(73,'Nought and Nought','Dismiss a batsman for a duck twice in a Test Match in single player or ranked online match.',4,1,72.17,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(74,'King Pair','Dismiss a batsman for a golden duck twice in a Test Match in single player or ranked online match.',3,1,68.73,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(75,'Ashes Destroyer','Take 40 wickets in an Ashes series with a single bowler in single player.',2,1,64.93,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(76,'Demolition with the Ball','Bowl the opponent out in 10 overs or less in single player or ranked online match.',3,1,73.77,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(77,'ODI Domination','Score 300 runs or more in an innings of an ODI Match in single player or ranked online match.',3,1,67.81,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(78,'Test Match Domination','Score 600 runs or more in an innings of a Test Match in single player or ranked online match.',2,1,68.92,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(79,'10 Wicket Defeat','Win a Test Match by 10 wickets in single player or ranked online match.',3,1,65.85,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(80,'Innings Defeat','Win a Test Match by an innings or more in single player or ranked online match.',3,1,71.25,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(81,'ODI Champions','Win a full-length ODI Tournament in single player.',2,1,67.87,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(82,'20 Over Champions','Win a full-length 20 Over Tournament in single player.',2,1,66.52,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(83,'Ashes Champions','Win an Ashes series in single player.',2,1,65.66,2,'2017-09-13 13:52:40','2017-09-13 13:52:40'),(84,'Winner Takes All','Earn all the trophies.',1,1,17.08,3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(85,'Welcome to Panau','Complete story mission 1.',4,1,92.79,3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(86,'Casino Bust','Complete story mission 2.',4,1,83.59,3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(87,'The White Tiger','Complete story mission 3.',4,1,57.22,3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(88,'Mountain Rescue','Complete story mission 4.',4,1,49.07,3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(89,'Three Kings','Complete story mission 5.',3,1,43.49,3,'2017-09-13 13:52:41','2017-09-13 13:52:41'),(90,'Into the Den','Complete story mission 6.',3,1,39.03,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(91,'A Just Cause','Complete story mission 7.',2,1,38.90,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(92,'Top Agent','Bonus for completing the game on Normal difficulty.',4,1,32.30,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(93,'Heroic Agent','Bonus for completing the game on Experienced difficulty.',3,1,21.37,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(94,'Legendary Agent','Bonus for completing the game on Hardcore difficulty.',2,1,20.06,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(95,'Gaining a foothold','Complete 3 stronghold takeovers.',4,1,59.91,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(96,'Conqueror of Panau','Complete 9 stronghold takeovers.',3,1,36.09,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(97,'A Trusted Ally','Complete 49 faction missions.',2,1,26.79,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(98,'First Taste of Chaos','Cause chaos for the first time.',4,1,96.69,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(99,'Saboteur','Complete 150 sabotages.',4,1,56.21,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(100,'Destroyer','Complete 1000 sabotages.',4,1,26.95,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(101,'Professional Hitman','Assassinate 25 colonels.',4,1,25.64,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(102,'Up to the Challenge 1','Complete 10 challenges.',4,1,29.54,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(103,'Up to the Challenge 2','Complete 50 challenges.',4,1,20.33,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(104,'Leaving No Rock Unturned','Collect 1000 resource items.',4,1,23.97,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(105,'Finders Keepers','Collect 100 resource items.',4,1,58.98,3,'2017-09-13 13:52:42','2017-09-13 13:52:42'),(106,'Faction Benefactor','Collect 150 faction items.',4,1,22.09,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(107,'Globetrotter','Discover 100 locations.',4,1,43.54,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(108,'Freeroamer 1','Reach 100% complete in 15 locations.',4,1,44.57,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(109,'Freeroamer 2','Reach 100% complete in 100 locations.',3,1,25.64,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(110,'Body Count','Kill 750 enemies.',4,1,52.31,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(111,'Unarmed and Dangerous','Kill 50 enemies using melee attacks.',4,1,44.00,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(112,'Gravity is a Bitch!','Kill 30 enemies by using the grappling hook and making them fall to their death.',4,1,43.77,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(113,'Follow Me!','Kill 5 enemies by dragging them behind a vehicle with the grappling hook.',4,1,32.57,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(114,'Hang \'em High!','Kill 30 enemies while they\'re suspended in the air with the grappling hook.',4,1,26.04,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(115,'Wrecking ball','Kill 5 enemies by smashing them with an object tethered to your vehicle with the grappling hook.',4,1,20.04,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(116,'Piñata Party','Kill 5 enemies with the melee attack while they\'re suspended with the grappling hook.',4,1,31.79,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(117,'Juggler','Kill 30 enemies while they\'re falling through the air.',4,1,32.27,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(118,'Road Rage','Kill 30 enemies by mowing them down with vehicles.',4,1,29.86,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(119,'Marksman','Kill 50 enemies by headshots.',4,1,56.74,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(120,'Killing Frenzy','Kill 20 enemies in 60 seconds.',4,1,39.21,3,'2017-09-13 13:52:43','2017-09-13 13:52:43'),(121,'Invincible Warrior','Kill 50 enemies in a row with inventory weapons without losing health.',4,1,30.07,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(122,'Destruction Frenzy','Destroy 30 objects in 60 seconds.',4,1,80.38,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(123,'Test Driver','Drive 30 different vehicles.',4,1,64.07,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(124,'Trying Everything Once','Drive all 104 vehicles.',4,1,18.97,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(125,'Road Trip','Travel 75 kilometers by land vehicle.',4,1,52.77,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(126,'Please Step Out of the Vehicle','Hijack 50 enemy vehicles.',4,1,31.12,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(127,'Stunt Driver','Get 100 stunt driver points.',4,1,35.87,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(128,'Halfway there','Reach 50% completion in the normal mode or mercenary mode.',4,1,24.21,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(129,'Parachute Climber','Open the parachute and then land on foot 300 meters above the starting height.',4,1,42.31,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(130,'I Believe I Can Fly','Base jump 1000 meters.',4,1,37.16,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(131,'Bridge Limbo','Fly an airplane under 30 unique bridges in Panau.',4,1,21.52,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(132,'Stunt Flyer','Fly an airplane close to the ground for 30 seconds.',4,1,27.73,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(133,'Perfectionist','Reach 75% completion in the normal mode or mercenary mode.',4,1,19.31,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(134,'Top of the World','Stand on foot at the highest point of Panau.',4,1,44.62,3,'2017-09-13 13:52:44','2017-09-13 13:52:44'),(135,'Platinum','Platinum',1,1,36.86,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(136,'First Treasure','Find one treasure',4,1,97.09,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(137,'Apprentice Fortune Hunter','Find ten treasures',4,1,89.52,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(138,'Novice Fortune Hunter','Find 20 treasures',4,1,79.31,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(139,'Cadet Fortune Hunter','Find 30 treasures',4,1,65.80,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(140,'Intermediate Fortune Hunter','Find 40 treasures',4,1,57.11,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(141,'Practiced Fortune Hunter','Find 50 treasures',4,1,51.99,4,'2017-09-13 13:52:47','2017-09-13 13:52:47'),(142,'Proficient Fortune Hunter','Find 60 treasures',4,1,48.95,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(143,'Professional Fortune Hunter','Find 70 treasures',4,1,47.14,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(144,'Expert Fortune Hunter','Find 80 treasures',4,1,45.80,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(145,'Crack Fortune Hunter','Find 90 treasures',4,1,44.75,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(146,'Master Fortune Hunter','Find all 100 treasures',3,1,43.77,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(147,'Relic Finder','Find the Strange Relic',4,1,55.37,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(148,'Survivor','Defeat 75 enemies in a row without dying',3,1,56.94,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(149,'20 Headshots','Defeat 20 enemies with headshots',4,1,91.37,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(150,'100 Headshots','Defeat 100 enemies with headshots',4,1,83.02,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(151,'250 Headshots','Defeat 250 enemies with headshots',3,1,51.96,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(152,'Headshot Expert','Defeat five enemies in a row with headshots',4,1,67.71,4,'2017-09-13 13:52:48','2017-09-13 13:52:48'),(153,'Run-and-Gunner','Defeat 20 enemies by shooting from the hip (without aiming with L1)',4,1,61.71,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(154,'Hangman','Defeat 20 enemies with gunfire by aiming while hanging',4,1,83.56,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(155,'50 Kills: 92FS - 9mm','Defeat 50 enemies with the 92FS - 9mm',4,1,82.96,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(156,'50 Kills: Micro - 9mm','Defeat 50 enemies with the Micro - 9mm',4,1,58.29,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(157,'30 Kills: Wes - 44','Defeat 30 enemies with the Wes - 44',4,1,56.56,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(158,'30 Kills: Desert - 5','Defeat 30 enemies with the Desert - 5',4,1,61.01,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(159,'20 Kills: Pistole','Defeat 20 enemies with the Pistole',4,1,71.24,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(160,'70 Kills: FAL','Defeat 70 enemies with the FAL',4,1,62.92,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(161,'70 Kills: M4','Defeat 70 enemies with the M4',4,1,84.25,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(162,'50 Kills: Dragon Sniper','Defeat 50 enemies with the Dragon Sniper',4,1,71.49,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(163,'30 Kills: Moss - 12','Defeat 30 enemies with the Moss - 12',4,1,58.39,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(164,'70 Kills: SAS - 12','Defeat 70 enemies with the SAS - 12',4,1,51.29,4,'2017-09-13 13:52:49','2017-09-13 13:52:49'),(165,'50 Kills: M32 - Hammer','Defeat 50 enemies with the M32 - Hammer',4,1,53.41,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(166,'30 Kills: RPG - 7','Defeat 30 enemies with the RPG - 7',4,1,53.30,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(167,'200 Kills: GAU - 19','Defeat 200 enemies with the GAU - 19',4,1,42.05,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(168,'30 Kills: Mk-NDI','Defeat 30 enemies with Mk-NDI grenades',4,1,84.21,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(169,'Triple Dyno-Might!','Defeat three enemies with one explosion',4,1,89.05,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(170,'Grenade Hangman','Defeat ten enemies with grenades by aiming while hanging',4,1,50.94,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(171,'Bare-knuckle Brawler','Defeat 20 enemies with hand-to-hand combat',4,1,93.86,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(172,'Bare-knuckle Slugger','Defeat 50 enemies with hand-to-hand combat',3,1,90.22,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(173,'Bare-knuckle Expert','Defeat ten enemies in a row with hand-to-hand combat',3,1,60.13,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(174,'Master Ninja','Defeat 50 enemies with stealth attacks',3,1,69.26,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(175,'Steel Fist Master','Defeat 20 enemies with a single punch, after softening them up with gunfire',4,1,56.69,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(176,'Steel Fist Expert','Defeat ten enemies in a row with a single punch, after softening them up with gunfire',3,1,41.38,4,'2017-09-13 13:52:50','2017-09-13 13:52:50'),(177,'Charted! - Easy','Finish the game in Easy Mode',3,1,76.40,4,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(178,'Charted! - Normal','Finish the game in Normal Mode',2,1,70.16,4,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(179,'Charted! - Hard','Finish the game in Hard Mode',2,1,48.22,4,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(180,'Charted! - Crushing','Finish the game in Crushing Mode',2,1,38.63,4,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(181,'Thrillseeker','Complete one Competitive Multiplayer game',4,1,64.45,4,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(182,'Buddy System','Complete one Cooperative Multiplayer game',4,1,55.71,4,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(183,'Sneaky…','Get 50 Stealth Kills',4,1,4.74,5,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(184,'Brass Knuckles','Get 100 Melee Kills',4,1,7.51,5,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(185,'Tastes Like Chicken','Get 20 BBQ Medals',4,1,4.39,5,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(186,'Rapid Fire!','Get 7 Tripled Medals in Deathmatch',4,1,4.64,5,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(187,'I Told You I Was Hardcore','Complete All 3 Co-op Objective Maps on Hard',4,1,4.83,5,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(188,'Made It','Finish Wave 10 in 1 Survival Game',4,1,8.58,5,'2017-09-13 13:52:51','2017-09-13 13:52:51'),(189,'Gold Digger','Finish Wave 10 in 1 Gold Rush Game',4,1,7.54,5,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(190,'Wipe Them Out…','Win 50 Deathmatch Games',4,1,7.24,5,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(191,'Plays Well With Others','Win 50 Objective Games',4,1,5.13,5,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(192,'Still Alive','Win 50 Elimination Games',4,1,4.51,5,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(193,'Plundered!','Get 200 Captured Medals',4,1,4.99,5,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(194,'Cold Blooded','Get 2500 Kills',3,1,5.18,5,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(195,'You Can\'t Break Me','Complete all three Co-op Objective maps on Crushing',4,1,4.33,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(196,'Under Siege','Finish Wave 10 in one Siege Game',4,1,7.10,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(197,'Speedy','Get 10 First! Medals',4,1,7.49,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(198,'I\'ll Cover You','Get 50 Defender Medals',4,1,7.59,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(199,'Fallen Angel','Get 50 Afterlife Medals',4,1,6.04,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(200,'Not So Fast','Get 20 Shut \'Em Down Medals',4,1,7.28,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(201,'Jack of All Trades','Get 15 Triple Threat Medals',4,1,4.94,6,'2017-09-13 13:52:52','2017-09-13 13:52:52'),(202,'You Run, I\'ll Shoot','Get 10 Protectorate Medals',4,1,3.48,6,'2017-09-13 13:52:53','2017-09-13 13:52:53'),(203,'Back at Ya','Get 50 Retaliation Medals',4,1,9.47,6,'2017-09-13 13:52:53','2017-09-13 13:52:53'),(204,'Kneel Before Z...','Get 5 Put \'Em Down Medals',4,1,5.71,6,'2017-09-13 13:52:53','2017-09-13 13:52:53'),(205,'Rock A Rhyme','Get 100 Tricky Medals',3,1,4.46,6,'2017-09-13 13:52:53','2017-09-13 13:52:53'),(206,'Platinum Trophy','Earn all available trophies for Crysis®',1,1,50.32,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(207,'A Little Trouble Parking','Discover the fate of the Lusca\'s Call',4,1,88.70,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(208,'Easy Darlin\'','Rescue the hostage',4,1,81.84,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(209,'You Knew, Didn\'t You?','Regroup with Prophet upriver',4,1,78.40,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(210,'Very Strange Readings','Infiltrate the excavation site',4,1,74.87,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(211,'Livin\' Up To Your Name','Board the VTOL for extraction',4,1,72.69,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(212,'Pro-Aircraft','Destroy all AA guns around the harbor',4,1,71.40,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(213,'Enjoy The Fireworks','Destroy the cruiser',4,1,71.21,7,'2017-09-13 13:52:58','2017-09-13 13:52:58'),(214,'Empty Platform','Secure the train station',4,1,69.56,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(215,'You\'re On Your Own','Proceed to the mining complex',4,1,70.27,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(216,'One Careful Owner','Reach the end of \'Onslaught\' in the tank you started with',4,1,58.52,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(217,'Going Underground','Enter the mines',4,1,69.58,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(218,'It\'s On Like General Kyong','Defeat General Kyong',4,1,69.51,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(219,'I\'m Coming Home','Escape the mysterious structure under the mountain',4,1,68.68,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(220,'Expedition Team','Escort Prophet to safety',4,1,68.37,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(221,'I\'m A Marine, Son!','Help the marines evacuate',4,1,68.22,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(222,'Strickland Would Be Proud','Defeat the flight deck invader',4,1,67.75,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(223,'Close Encounter','Secure victory in the Battle of Lingshan',4,1,67.28,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(224,'Delta: Act I','Complete \'Contact\', \'Recovery\' and \'Relic\' on Delta difficulty',3,1,52.43,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(225,'Delta: Act II','Complete \'Assault\', \'Onslaught\' and \'Awakening\' on Delta difficulty',3,1,51.13,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(226,'Delta: Act III','Complete \'Core\', \'Paradise Lost\', \'Exodus\' and \'Reckoning\' on Delta difficulty',3,1,50.82,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(227,'Crysis Controlled','Complete the game on any difficulty',2,1,67.22,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(228,'Cool In A Crysis','Complete the game on Hard or Delta difficulty',2,1,51.92,7,'2017-09-13 13:52:59','2017-09-13 13:52:59'),(229,'Following Orders','Complete 3 Secondary Objectives',4,1,81.40,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(230,'Without Question','Complete 6 Secondary Objectives',3,1,69.91,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(231,'Perfect, Soldier!','Complete all Secondary Objectives',2,1,63.70,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(232,'No Fly Zone','Destroy 5 helicopters',3,1,69.99,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(233,'Tank Buster','Destroy 5 enemy tanks',4,1,70.78,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(234,'This Is My Rifle','Customize a weapon to use all 5 modification points',4,1,61.65,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(235,'Special Forces','Kill 200 enemies',4,1,74.30,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(236,'Team Raptor','Kill 400 enemies',3,1,68.34,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(237,'Long Distance Relationship','Kill an enemy 200m away',4,1,71.86,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(238,'Weapons Master','Perform a kill with every firearm',3,1,64.87,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(239,'Something For Every Occasion','Use all weapon attachments',4,1,65.09,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(240,'Choke Hold','Kill 20 enemies with grab',3,1,62.92,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(241,'Marathon Man','Speed sprint 3km',3,1,77.37,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(242,'Nano Ninja','Perform 5 consecutive kills without being spotted by an enemy',4,1,68.30,7,'2017-09-13 13:53:00','2017-09-13 13:53:00'),(243,'Knock-off Knockout','Kill an enemy Nanosuit soldier with a Strength punch',4,1,59.88,7,'2017-09-13 13:53:01','2017-09-13 13:53:01'),(244,'Zoology','Pick up an animal',4,1,80.72,7,'2017-09-13 13:53:01','2017-09-13 13:53:01'),(245,'Keen Observer','Tag 30 enemies using the binoculars',3,1,65.51,7,'2017-09-13 13:53:01','2017-09-13 13:53:01'),(246,'Catch This!','Kill 10 enemies by throwing an object at them',4,1,55.94,7,'2017-09-13 13:53:01','2017-09-13 13:53:01');
/*!40000 ALTER TABLE `trophies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `trophy_user`
--

LOCK TABLES `trophy_user` WRITE;
/*!40000 ALTER TABLE `trophy_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `trophy_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Platinum','2017-08-24 11:06:18','2017-08-24 11:06:18'),(2,'Gold','2017-08-24 11:06:18','2017-08-24 11:06:18'),(3,'Silver','2017-08-24 11:06:18','2017-08-24 11:06:18'),(4,'Bronze','2017-08-24 11:06:18','2017-08-24 11:06:18');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `videoables`
--

LOCK TABLES `videoables` WRITE;
/*!40000 ALTER TABLE `videoables` DISABLE KEYS */;
/*!40000 ALTER TABLE `videoables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-13 14:22:55
