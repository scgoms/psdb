<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Admin Interface Routes
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function()
{
  // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('posts', 'Admin\PostsCrudController');
    CRUD::resource('tags', 'Admin\TagsCrudController');
    CRUD::resource('links', 'Admin\LinksCrudController');  
    CRUD::resource('reports', 'Admin\ReportsCrudController');
    CRUD::resource('games', 'Admin\GamesCrudController');
    CRUD::resource('groups', 'Admin\GroupsCrudController');
    CRUD::resource('trophies', 'Admin\TrophiesCrudController');
  // [...] other routes
});

Route::get('/', 'PostsController@index');
Route::get('/search', 'SearchController@search');
Route::get('/posts/{post}', 'PostsController@show');

Route::get('/games', 'GamesController@index');
Route::get('/games/{game}', 'GamesController@show');
Route::get('/games/{game}/images', 'GameImagesController@index');
Route::post('/games/{game}/comment', 'GameCommentsController@store')->middleware('auth');
Route::post('/games/{game}/image', 'GameImagesController@store')->middleware('auth');
Route::post('/games/{game}/video', 'GameVideosController@store')->middleware('auth');

Route::get('/groups', 'GroupsController@index');
Route::get('/groups/{group}', 'GroupsController@show');
Route::get('/games/{game}/images', 'GameImagesController@index');
Route::post('/groups/{group}/comment', 'GroupCommentsController@store')->middleware('auth');
Route::post('/groups/{group}/image', 'GroupImagesController@store')->middleware('auth');
Route::post('/groups/{group}/video', 'GroupVideosController@store')->middleware('auth');

Route::get('/trophies', 'TrophiesController@index');
Route::get('/trophies/{trophy}', 'TrophiesController@show');
Route::get('/games/{game}/images', 'GameImagesController@index');
Route::post('/trophies/{trophy}/comment', 'TrophyCommentsController@store')->middleware('auth');
Route::post('/trophies/{trophy}/image', 'TrophyImagesController@store')->middleware('auth');
Route::post('/trophies/{trophy}/video', 'TrophyVideosController@store')->middleware('auth');

Route::post('/comments/{comment}/report', 'CommentReportsController@store');

Route::get('/profile', 'ProfileController@index');
Route::get('/profile/settings', 'ProfileController@edit');
Route::get('/profile/{user}', 'ProfileController@show');
Route::post('/profile/{user}', 'ProfileController@update');