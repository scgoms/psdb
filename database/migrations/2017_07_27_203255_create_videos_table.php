<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('youtube_id');
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
        });
        Schema::create('videoables', function(Blueprint $table){
            $table->integer('video_id');
            $table->integer('videoable_id');
            $table->string('videoable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
        Schema::dropIfExists('videoables');
    }
}
