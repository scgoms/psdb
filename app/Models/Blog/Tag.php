<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\Models\Blog\Post;

class Tag extends Model
{
    use CrudTrait;
    protected $fillable =	['name', 'type'];

    public function posts()
    {
    	return $this->belongsToMany(Post::class);
    }
}
