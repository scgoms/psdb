<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Generic\User;
use App\Models\Blog\Tag;
use App\Models\Blog\Comment;
use Carbon\Carbon;
use Extractor;


class Post extends Model
{
    use CrudTrait;
    protected $fillable =	['title', 'body', 'user_id'];

    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public static function archives()
    {
    	return Post::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
    		->groupBy('year', 'month')
    		->orderByRaw('min(created_at) desc')
    		->get();
    }

    public function scopeFilterByMonth($query, $filters)
    {
        if($month = $filters['month']){
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if($year = $filters['year']){
            $query->whereYear('created_at', $year);
        }
    }

    public function scopeFilterByTag($query, $filters)
    {
        if(!is_null($filters['tag'])){
            $query->join('post_tag', 'post_id', '=', 'id')
            ->where('post_tag.tag_id', $filters['tag']);
        }
    }

    public function extract()
    {
        return Extractor::getExtract($this->body, 300);
    }

}
