<?php

namespace App\Models\PSDB;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
    	'title'
    ];
    
}
