<?php

namespace App\Models\PSDB;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\Models\PSDB\Game;
use App\Models\PSDB\Trophy;
use App\Models\Generic\Comment;
use App\Models\Generic\Image;
use App\Models\Generic\Video;

class Group extends Model
{
    use CrudTrait;
    protected $fillable = [
        'title',
        'description',
        'game_id',
    ];

    
    public function game()
    {
    	return $this->belongsTo(Game::class);
    }

    public function trophies()
    {
    	return $this->hasMany(Trophy::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function videos()
    {
        return $this->morphMany(Video::class, 'videoable');
    }
}
