<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Model;
use App\Models\Generic\Report;
use App\Models\PSDB\Game;
use App\Models\PSDB\Group;
use App\Models\PSDB\Trophy;
use App\Models\Generic\User;

class Video extends Model
{
	protected $fillable = [
		'youtube_id'
	];
	
	public function videos()
	{
		return $this->morphTo();
	}

	public function reports()
	{
		return $this->morphMany(Report::class, 'reportable');
	}

	public function users()
	{
		return $this->belongsTo(User::class);
	}

	public function link()
	{
		switch($this->commentable_type){
            case('App\Models\PSDB\Game'):
                return '/games/' . $this->commentable_id;
                break;
            case('App\Models\PSDB\Group'):
            	return '/groups/' . $this->commentable_id;
                break;
            case('App\Models\PSDB\Trophy'):
            	return '/trophies/' . $this->commentable_id;
                break;
            case('App\Models\Blog\Post'):
            	return '/posts/' . $this->commentable_id;
            	break;
        }
	}
}
