<?php

namespace App\Models\Generic;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use App\Models\Generic\Comment;
use App\Models\Generic\Video;
use App\Models\Generic\Image;
use App\Models\PSDB\Game;
use App\Models\PSDB\Trophy;
use Auth;

use Backpack\CRUD\CrudTrait; 
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use CrudTrait; 
    use HasRoles; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'psn_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function games()
    {
        return $this->belongsToMany(Game::class);
    }

    public function trophies()
    {
        return $this->belongsToMany(Trophy::class);
    }

    public function progress(Game $game)
    {
        $t = count($game->trophies);
        $c = count($game->trophies()->whereHas('users', function($query){
            $query->where('id', '=', 1);
        })->get());
        return intval(round(($c/$t)*100));
    }

    public function trophyCount(Game $game)
    {
        return count($game->trophies()->whereHas('users', function($query){
            $query->where('id', '=', 1);
        })->get());
    }

        /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
