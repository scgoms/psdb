<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\PSDB\Game;
use App\Models\PSDB\Group;
use App\Models\PSDB\Trophy;
use App\Models\PSDB\Platform;
use App\Models\PSDB\Type;
use App\Models\Generic\Image;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use PSNAPI;
use Illuminate\Http\File;

use Illuminate\Support\Facades\DB;

class UpdateGame implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $psn_api;
    protected $npcommid;


    public function __construct($npcommid)
    {
        $this->npcommid = $npcommid;        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Running Update Game Job::' . $this->npcommid);
        $this->psn_api = new PSNAPI();
        // $bar = $this->output->createProgressBar(count($update_list));
        // $bar->advance();
        // $update_list = array(
        //     array('npcommid'=> 'NPWR12997_00'),
        //     array('npcommid'=> 'NPWR12476_00'),
        //     array('npcommid'=> 'NPWR11556_00'),
        //     array('npcommid'=> 'NPWR07290_00')
        // );
        $num_of_attempts = 0;
        try{
            do{
                DB::connection()->getPdo()->beginTransaction();
                $api_data = $this->psn_api->getCompleteDataSet($this->npcommid);
                $local_game = $this->addGame($api_data['game']);
                $this->storeImage('images/' . $local_game->id . '/', 'game_logo', $api_data['game']['image']);
                if(isset($api_data['game']['screenshots'])){
                    $this->addScreenshots($local_game, $api_data['game']['screenshots']);
                }
                $this->attachGameToPlatforms($local_game, $api_data['game']['pf']);
                $local_groups = array();
                foreach($api_data['groups'] as $group){
                    $local_group = $this->addGroup($group, $local_game->id);
                    $local_groups[] = $local_group;
                    $this->storeImage('images/' . $local_group->game->id . '/' . $local_group->id . '/', 'group_logo', $group['image']);
                }
                $local_trophies = array();
                foreach($api_data['trophies'] as $trophy){
                    $local_trophy = $this->addTrophy($trophy, $local_groups[$trophy['group']]->id);
                    $local_trophies[] = $local_trophy;
                    $this->storeImage('images/' . $local_trophy->group->game->id . '/' . $local_trophy->group->id . '/' . $local_trophy->id . '/', 'trophy_logo', $trophy['image']);   
                }
                // $bar->advance();
                DB::connection()->getPdo()->commit();
                break;
            }while ($num_of_attempts < 5);
            $num_of_attempts = 0;
        }catch(\Exception $e){
            $num_of_attempts = $num_of_attempts + 1;
            DB::connection()->getPdo()->rollBack();
        }
        Log::debug('Finished running Update Game Job::' . $this->npcommid);

        // $bar->finish();
    }

    private function sanitize($string){ //Move this to Extractor and rename extractor to string method or something
        $string = str_replace("©", " ", $string);
        $string = str_replace("℗", " ", $string);
        $string = str_replace("®", " ", $string);
        $string = str_replace("℠", " ", $string);
        $string = str_replace("™", " ", $string);
        $string = str_replace("  ", " ", $string);
        $string = trim($string);
        return $string;
    }




    

    /**
        Takes a game array as provided by the API and adds relevant details to our database
        Returns the game object
    */
    private function addGame($api_game){
        //Sanitize basic information about the game
        $title = $this->sanitize($api_game['title']);
        if(strlen(trim($api_game['english']))){
            $english_title = $this->sanitize($api_game['english']);
        }else{
            $english_title = $title;
        }
        return Game::create([
            'title' =>  $title,
            'npcommid'  =>  $api_game['npcommid'],
            'english_title' =>  $english_title
        ]);
    }

    /**
        Attaches game to any platforms it requires.
        $pf is the string of platforms as provided by the PSNAPI
    */
    private function attachGameToPlatforms($game, $pf){
        $platforms = explode(',', $pf);
        $local_platforms = Platform::all();
        foreach($platforms as $platform){
            switch(trim($platform)){
                case 'ps3':
                    $local_platforms->where('title', 'Playstation 3')->first()->games()->attach($game);
                    break;
                case 'ps4':
                    $local_platforms->where('title', 'Playstation 4')->first()->games()->attach($game);
                    break;
                case 'psp2':
                    $local_platforms->where('title', 'Playstation Vita')->first()->games()->attach($game);
                    break;
            }
        }
    }

    /**
        Takes a group array as provided by the API and adds relevant details to our database
        Returns the group object
    */
    private function addGroup($group, $game_id){
        return Group::create([
            'title' =>  $this->sanitize($group['title']),
            'description'=> $this->sanitize($group['description']),
            'game_id'  =>  $game_id
        ]);
    }

    /**
        Takes a trophy array as provided by the API and adds relevant details to our database
        Returns the trophy object
    */
    private function addTrophy($trophy, $group_id){
        //Perform some basic sanitization, a trophies rarity can't be over 100%
        if($trophy['percentage_obtained']>100){
            $trophy['percentage_obtained'] = 100;
        }
        if($trophy['hidden']){
            $trophy['hidden'] = 1;
        }else{
            $trophy['hidden'] = 0;
        }        
        $trophy_types = Type::all();
        switch($trophy['type']){
            case 'PLATINUM':
                $type_id = $trophy_types->where('title', 'Platinum')->first()->id;
                break;
            case 'GOLD':
                $type_id = $trophy_types->where('title', 'Gold')->first()->id;
                break;
            case 'SILVER':
                $type_id = $trophy_types->where('title', 'Silver')->first()->id;
                break;
            case 'BRONZE':
                $type_id = $trophy_types->where('title', 'Bronze')->first()->id;
                break;
        }
        return Trophy::create([
            'title' =>  $trophy['title'],
            'description'   =>  $trophy['description'],
            'type_id'  =>  $type_id,
            'group_id'  =>  $group_id,
            'hidden'    =>  $trophy['hidden'],
            'rarity'    =>  $trophy['percentage_obtained']
        ]);
    }

    private function addScreenshots($game, $screenshots)
    {
        if(!is_array($screenshots)){
            $s = $screenshots;
            $screenshots = array($s);
        }
        foreach($screenshots as $screenshot){
            $extension = pathinfo($screenshot, PATHINFO_EXTENSION);
            $filename = 'tmp' . '.' . $extension;
            $file = file_get_contents($screenshot);
            $save = file_put_contents($filename, $file);
    
            if($save){
                $path = Storage::disk('s3')->putFile('images' . '/' . $game->id . '/images', new File($filename));
                $image = Image::create([
                    'user_id'   =>  1,
                    'src'  =>  $path
                ]);    
                $game->images()->save($image);    
                $game->save();
            }
        }
        // $path = Storage::putFile('avatars', $request->file('avatar'));
    }

    private function storeImage($path, $file_name, $image_url)
    {
        $extension = pathinfo($image_url, PATHINFO_EXTENSION);
        $filename = 'tmp' . '.' . $extension;
        $file = file_get_contents($image_url);
        $save = file_put_contents($filename, $file);

        if($save){
            Storage::disk('s3')->putFileAs($path, new File($filename), $file_name . '.' . $extension);
        }
    }
}
