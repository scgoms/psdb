<?php

namespace App\Classes;

class Extractor {
	public function getExtract($body, $length){
		$truncatedText = substr($body, $length);
	    $pos = strpos($truncatedText, ">");
	    if($pos !== false)
	    {
	        $body = substr($body, 0,$length + $pos + 1);
	    }
	    else
	    {
	        $body = substr($body, 0,$length);
	    }

	    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $body, $result);
	    $openedtags = $result[1];

	    preg_match_all('#</([a-z]+)>#iU', $body, $result);
	    $closedtags = $result[1];

	    $len_opened = count($openedtags);

	    if (count($closedtags) == $len_opened)
	    {
	        return $body;
	    }

	    $openedtags = array_reverse($openedtags);
	    for ($i=0; $i < $len_opened; $i++)
	    {
	        if (!in_array($openedtags[$i], $closedtags))
	        {
	            $body .= '</'.$openedtags[$i].'>';
	        }
	        else
	        {
	            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
	        }
	    }
	    return $body;
	}
}