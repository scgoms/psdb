<?php

namespace App\Http\Controllers;

use App\Models\Generic\Image;
use App\Models\PSDB\Game;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class GameImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Game $game)
    {
        return $game->images;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Game $game, Request $request)
    {
        $this->validate(request(), [
            'image' => 'required'
        ]);
        $image = Image::create([
            'user_id'   =>  Auth::id(),
            'src'  =>  $request->file('image')->store("/images/{$game->id}/images", 's3')
        ]);

        $game->images()->save($image);

        $game->save();

        return back();
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Generic\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Generic\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Generic\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Generic\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
