<?php

namespace App\Http\Controllers;

use App\Models\Generic\Comment;
use App\Models\PSDB\Trophy;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TrophyCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Trophy $trophy, Request $request)
    {
        $this->validate(request(), [
            'body' => 'required|min:20'
        ]);
        $comment = Comment::create([
            'body'  =>  $request->body,
            'user_id'   =>  Auth::id()
        ]);

        $trophy->comments()->save($comment);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Generic\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Generic\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Generic\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Generic\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
