<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class TrophiesCrudController extends CrudController
{
    public function setup()
    {
    	$this->crud->setModel("App\Models\PSDB\Trophy");
    	$this->crud->setRoute("admin/trophies");
    	$this->crud->setEntityNameStrings('trophy', 'trophies');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

    	$this->crud->setColumns(
        [
            [
                'name'  => 'title',
                'label' => 'Title',
                'type'  => 'text',
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Group',
                'type'      => 'select',
                'name'      => 'group_id', // the method that defines the relationship in your Model
                'entity'    => 'group', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Group", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Type',
                'type'      => 'select',
                'name'      => 'type_id', // the method that defines the relationship in your Model
                'entity'    => 'type', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Type", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
            ],
            [   // Number
                'name' => 'rarity',
                'label' => 'Percentage Obtained',
                'type' => 'number',
                // optionals
                'attributes' => ["step" => "any"], // allow decimals
                'suffix' => "%",
            ],
            [   // Checkbox
                'name' => 'hidden',
                'label' => 'Hidden',
                'type' => 'checkbox',
            ],
        ]);
    	$this->crud->addFields([
            [
        		'name'	=>	'title',
      			'label'	=>	'Title',
                'tab'   =>  'Trophy'
            ],
            [
                'name'  =>  'description',
                'type'  =>  'ckeditor',
                'label' =>  'Description',
                'tab'   =>  'Trophy'
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Group',
                'type'      => 'select2',
                'name'      => 'group_id', // the method that defines the relationship in your Model
                'entity'    => 'group', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Group", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
                'tab'   =>  'Trophy'
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Type',
                'type'      => 'select2',
                'name'      => 'type_id', // the method that defines the relationship in your Model
                'entity'    => 'type', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Type", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
                'tab'   =>  'Trophy'
            ],
            [   // Number
                'name' => 'rarity',
                'label' => 'Percentage Obtained',
                'type' => 'number',
                // optionals
                'attributes' => ["step" => "any"], // allow decimals
                'suffix' => "%",
                'tab'   =>  'Trophy'
            ],
            [   // Checkbox
                'name' => 'hidden',
                'label' => 'Hidden',
                'type' => 'checkbox',
                'tab'   =>  'Trophy'
            ],
    	]);
    	
    }

    public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
