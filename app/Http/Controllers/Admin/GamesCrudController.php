<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class GamesCrudController extends CrudController
{
    public function setup()
    {
    	$this->crud->setModel("App\Models\PSDB\Game");
    	$this->crud->setRoute("admin/games");
    	$this->crud->setEntityNameStrings('game', 'games');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

    	$this->crud->setColumns(
        [
            [
                'name'  => 'title',
                'label' => 'Title',
                'type'  => 'text',
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Groups',
                'type'      => 'select_multiple',
                'name'      => 'groups', // the method that defines the relationship in your Model
                'entity'    => 'groups', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Group", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
            ],
        ]);
    	$this->crud->addFields([
            [
        		'name'	=>	'title',
      			'label'	=>	'Title',
                'tab'   =>  'Game'
            ],
            [
                'name'  =>  'english_title',
                'label' =>  'Title (English)',
                'tab'   =>  'Game'
            ],
            [
                'name'  =>  'npcommid',
                'label' =>  'NPCOMMID',
                'tab'   =>  'Game'
            ],
            [
                'name'  =>  'description',
                'type'  =>  'ckeditor',
                'label' =>  'Description',
                'tab'   =>  'Game'
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Groups',
                'type'      => 'select2_multiple',
                'name'      => 'groups', // the method that defines the relationship in your Model
                'entity'    => 'groups', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Group", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
                'tab'   =>  'Game'
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Platform',
                'type'      => 'select2_multiple',
                'name'      => 'platforms', // the method that defines the relationship in your Model
                'entity'    => 'platforms', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Platform", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
                'tab'   =>  'Game'
            ],
    	]);
    	
    }

    public function store(StoreRequest $request)
	{
        return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
