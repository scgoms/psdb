<?php

namespace App\Http\Controllers;

use App\Models\Generic\Video;
use App\Models\PSDB\Trophy;
use Illuminate\Http\Request;

class TrophyVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Trophy $trophy, Request $request)
    {
        $query = array();
        parse_str(parse_url($request->video, PHP_URL_QUERY), $query);

        $video = Video::create([
            'youtube_id' => $query['v']
        ]);

        $trophy->videos()->save($video);

        $trophy->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->request('GET','https://www.googleapis.com/youtube/v3/videos?id=' . $video->youtube_id . '&part=snippet&key=' . config('app.youtube_secret'));
        $video->thumbnail = json_decode($result->getBody()->getContents())->items[0]->snippet->thumbnails->high->url;
        return $video;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
    }
}
