<?php

namespace App\Http\Controllers;

use App\Models\Generic\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Events\UserRegisteredWithPSN;

class ProfileController extends Controller
{
    /**
     * Display the logged in users profile
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();        
        return view('profiles.index', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Generic\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('profiles.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Generic\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        $user = Auth::user();
        return view('profiles.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Generic\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if(Auth::id() != $user->id){
            return back();
        }
        else{
            $user->name = $request->username;
            if($user->psn_name != $request->psn_name){
                //Remove their old games and trophies associated on that account and re-queue get PSN Data
                foreach($user->trophies as $trophy){
                    $user->detach($trophy);
                }
                foreach($user->games as $game){
                    $user->detach($game);
                }
                $user->psn_name = $request->psn_name;
                event(new UserRegisteredWithPSN($user));
            }
            if($request->password){ //Check if new password has been entered, otherwise, skip this.
                $hashed_password = $user->password;
                if(Hash::check($request->old_password, $hashed_password)){
                    if($request->password == $request->confirm_password){
                        $user->fill([
                            'password' => Hash::make($request->password)
                        ])->save();
                        $request->session()->flash('success', 'Your password has been updated');
                    }else{
                        $request->session()->flash('failure', 'Your password has not been changed');
                    }
                }
                else{
                    $request->session()->flash('failure', 'Your password has not been changed');
                }
            }

            if($request->publish_comments ? $user->publish_comments = true : $user->publish_comments = false)
            if($request->publish_images ? $user->publish_images = true : $user->publish_images = false)
            if($request->publish_games ? $user->publish_games = true : $user->publish_games = false)
            $user->save();
            return back();
        }
    }
}
