<?php

namespace App\Http\Controllers;

use App\Models\Generic\Video;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        $client = new Client();
        $response = $client->request('GET', 'https://www.googleapis.com/youtube/v3/videos', [
            'query' =>  [
                            'part' => 'snippet',
                            'id'    =>  $video->youtube_id,
                            'key'   =>  config('app.youtube_secret')
            ]
        ]);
        $obj = json_decode($response->getBody());
        $video->youtube = $obj->items[0];
        return $video;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Generic\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
    }
}
