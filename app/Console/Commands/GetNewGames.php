<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PSDB\Game;
use App\Models\PSDB\Group;
use App\Models\PSDB\Trophy;
use App\Models\PSDB\Platform;
use App\Models\PSDB\Type;
use App\Models\Generic\Image;

use Illuminate\Support\Facades\Storage;

use PSNAPI;
use Illuminate\Http\File;

use Illuminate\Support\Facades\DB;

class GetNewGames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:games';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets a list of games to add from the PSNAPI and adds them to the system';

    private $psn_api;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->psn_api = new PSNAPI();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $update_list = $this->getUpdateList();
        // $bar = $this->output->createProgressBar(count($update_list));
        // $bar->advance();
        // $update_list = array(
        //     array('npcommid'=> 'NPWR12997_00'),
        //     array('npcommid'=> 'NPWR12476_00'),
        //     array('npcommid'=> 'NPWR11556_00'),
        //     array('npcommid'=> 'NPWR07290_00')
        // );
        $num_of_attempts = 0;
        foreach($update_list as $game_to_add){
            try{
                do{
                    DB::connection()->getPdo()->beginTransaction();
                    $api_data = $this->psn_api->getCompleteDataSet($game_to_add['npcommid']);
                    $local_game = $this->addGame($api_data['game']);
                    $this->storeImage('images/' . $local_game->id . '/', 'game_logo', $api_data['game']['image']);
                    if(isset($api_data['game']['screenshots'])){
                        $this->addScreenshots($local_game, $api_data['game']['screenshots']);
                    }
                    $this->attachGameToPlatforms($local_game, $api_data['game']['pf']);
                    $local_groups = array();
                    foreach($api_data['groups'] as $group){
                        $local_group = $this->addGroup($group, $local_game->id);
                        $local_groups[] = $local_group;
                        $this->storeImage('images/' . $local_group->game->id . '/' . $local_group->id . '/', 'group_logo', $group['image']);
                    }
                    $local_trophies = array();
                    foreach($api_data['trophies'] as $trophy){
                        $local_trophy = $this->addTrophy($trophy, $local_groups[$trophy['group']]->id);
                        $local_trophies[] = $local_trophy;
                        $this->storeImage('images/' . $local_trophy->group->game->id . '/' . $local_trophy->group->id . '/' . $local_trophy->id . '/', 'trophy_logo', $trophy['image']);   
                    }
                    // $bar->advance();
                    DB::connection()->getPdo()->commit();
                    break;
                }while ($num_of_attempts < 5);
                $num_of_attempts = 0;
            }catch(\Exception $e){
                echo "Rolling Back on game: " . $game_to_add['npcommid']. '\n';
                $num_of_attempts = $num_of_attempts + 1;
                DB::connection()->getPdo()->rollBack();
            }            
        }
        // $bar->finish();
    }

    private function sanitize($string){ //Move this to Extractor and rename extractor to string method or something
        $string = str_replace("©", " ", $string);
        $string = str_replace("℗", " ", $string);
        $string = str_replace("®", " ", $string);
        $string = str_replace("℠", " ", $string);
        $string = str_replace("™", " ", $string);
        $string = str_replace("  ", " ", $string);
        $string = trim($string);
        return $string;
    }


    /**
        Gets a list of games currently in the database and all games returned by the API, checks through each item in the
        remote game list to see if they've already been added to the system and if not, adds them to an array of games to 
        be updated.

        Once it has checked all those games, returns that array of games to be updated
    */
    private function getUpdateList(){
        $local_games = Game::all()->pluck('npcommid');
        $remote_games = $this->psn_api->getListOfGames();
        $update_games = array();

        foreach($remote_games['game'] as $game){
            if(!$local_games->contains($game['npcommid'])){
                $update_games[] = $game;
            }
        }
        return $update_games;
    }

    

    /**
        Takes a game array as provided by the API and adds relevant details to our database
        Returns the game object
    */
    private function addGame($api_game){
        //Sanitize basic information about the game
        $title = $this->sanitize($api_game['title']);
        if(strlen(trim($api_game['english']))){
            $english_title = $this->sanitize($api_game['english']);
        }else{
            $english_title = $title;
        }

        return Game::create([
            'title' =>  $title,
            'npcommid'  =>  $api_game['npcommid'],
            'english_title' =>  $english_title
        ]);
    }

    /**
        Attaches game to any platforms it requires.
        $pf is the string of platforms as provided by the PSNAPI
    */
    private function attachGameToPlatforms($game, $pf){
        $platforms = explode(',', $pf);
        $local_platforms = Platform::all();
        foreach($platforms as $platform){
            switch(trim($platform)){
                case 'ps3':
                    $local_platforms->where('title', 'Playstation 3')->first()->games()->attach($game);
                    break;
                case 'ps4':
                    $local_platforms->where('title', 'Playstation 4')->first()->games()->attach($game);
                    break;
                case 'psp2':
                    $local_platforms->where('title', 'Playstation Vita')->first()->games()->attach($game);
                    break;
            }
        }
    }

    /**
        Takes a group array as provided by the API and adds relevant details to our database
        Returns the group object
    */
    private function addGroup($group, $game_id){
        return Group::create([
            'title' =>  $this->sanitize($group['title']),
            'description'=>$this->sanitize($group['description']),
            'game_id'  =>  $game_id
        ]);
    }

    /**
        Takes a trophy array as provided by the API and adds relevant details to our database
        Returns the trophy object
    */
    private function addTrophy($trophy, $group_id){
        //Perform some basic sanitization, a trophies rarity can't be over 100%
        if($trophy['percentage_obtained']>100){
            $trophy['percentage_obtained'] = 100;
        }
        if($trophy['hidden']){
            $trophy['hidden'] = 0;
        }else{
            $trophy['hidden'] = 1;
        }        
        $trophy_types = Type::all();
        switch($trophy['type']){
            case 'PLATINUM':
                $type_id = $trophy_types->where('title', 'Platinum')->first()->id;
                break;
            case 'GOLD':
                $type_id = $trophy_types->where('title', 'Gold')->first()->id;
                break;
            case 'SILVER':
                $type_id = $trophy_types->where('title', 'Silver')->first()->id;
                break;
            case 'BRONZE':
                $type_id = $trophy_types->where('title', 'Bronze')->first()->id;
                break;
        }
        return Trophy::create([
            'title' =>  $trophy['title'],
            'description'   =>  $trophy['description'],
            'type_id'  =>  $type_id,
            'group_id'  =>  $group_id,
            'hidden'    =>  $trophy['hidden'],
            'rarity'    =>  $trophy['percentage_obtained']
        ]);
    }

    private function addScreenshots($game, $screenshots)
    {
        if(!is_array($screenshots)){
            $s = $screenshots;
            $screenshots = array($s);
        }
        foreach($screenshots as $screenshot){
            $extension = pathinfo($screenshot, PATHINFO_EXTENSION);
            $filename = 'tmp' . '.' . $extension;
            $file = file_get_contents($screenshot);
            $save = file_put_contents($filename, $file);
    
            if($save){
                $path = Storage::disk('s3')->putFile('images' . '/' . $game->id . '/images', new File($filename));
                $image = Image::create([
                    'user_id'   =>  1,
                    'src'  =>  $path
                ]);    
                $game->images()->save($image);    
                $game->save();
            }
        }
        // $path = Storage::putFile('avatars', $request->file('avatar'));
    }

    private function storeImage($path, $file_name, $image_url)
    {
        $extension = pathinfo($image_url, PATHINFO_EXTENSION);
        $filename = 'tmp' . '.' . $extension;
        $file = file_get_contents($image_url);
        $save = file_put_contents($filename, $file);

        if($save){
            Storage::disk('s3')->putFileAs($path, new File($filename), $file_name . '.' . $extension);
        }
    }
}