<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Backpack\PageManager\app\Models\Page;
use App\Models\Blog\Post;
use App\Models\Blog\Tag;
use App\Models\Blog\Link;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('layouts.sidebar', function($view) {
            $view->with('archives', Post::archives())->with('links', Link::all());
        });

        view()->composer('layouts.sidebar', function($view) {
            $view->with('tags', Tag::all());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
