Understand how we want our system to update
	What can PSNAPI push scripts do for us?
	Define our data requirements

Finish up reporting system
User Roles and Permissions
Set up paypal and patreon links
Search Tools
Thumbnail stuff

UPDATES
Ideally, things will queue and we can use push data to reduce our loads

US
When a user registers
	Create a user for them, fire off an event to get their PSN data - Done

Everyday
	Manual Check of PSNAPI for new games and update trophy stats.
		Two Scripts
		First of all, check for updated trophy stats
		Second of all, check for new games.

Unless we can figure out if a push notification will work for users, we will also need to check every nigvht for new user trophies. - Done

PUSH
	Game updates
	Group updates
	Trophy Updates

REPORTS
	Get it working on;
		x Images - Is added, need to make sure it's styled correctly
		x Videos - Is added, need to make sure it's styled correctly

USER ROLES AND PERMISSIONS
Basically just need roles for admin and moderators.
	Consider using permissions as well as roles to ensure high granular control

SEARCH TOOLS
For games/groups/trophies indexes.
	Basically want a sidebar with options relevant to index
	