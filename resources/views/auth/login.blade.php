@extends('layouts.master')

@section('content')
    <div class="card">
        <header class="card-header">
            <p class="card-header-title has-text-centered">
                Login
            </p>
        </header>
        <div class="card-content">
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="field">
                            <label for="email" class="label">E-mail Address</label>
                            <div class="control">
                                <input id="email" type="email" class="input @if($errors->has('email')) is-danger @endif" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        <small>{{ $errors->first('email') }}</small>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="field">
                            <label for="password" class="label">Password</label>
                            <div class="control">
                                <input id="password" type="password" class="input @if($errors->has('email')) is-danger @endif" name="password" required>

                                @if ($errors->has('password'))
                                    <p class="help is-danger">
                                        <small>{{ $errors->first('password') }}</small>
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <label class="checkbox">
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                                    Remember Me
                                </label>
                            </div>
                        </div>

                        <div class="field-is-grouped">
                            <div class="control">
                                <button type="submit" class="button is-primary">
                                    Login
                                </button>
                            </div>
                            <div class="control">
                                <a href="{{ route('password.request') }}" class="button is-link"></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>                
        </div>
    </div>
@endsection
