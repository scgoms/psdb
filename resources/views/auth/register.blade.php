@extends('layouts.master')

@section('content')
<div class="card">
    <header class="card-header">
        <p class="card-header-title has-text-centered">
            Register
        </p>
    </header>
    <div class="card-content">
        <div class="columns">
            <div class="column is-6 is-offset-3">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="field">
                        <label for="name" class="label">Name</label>
                        <div class="control">
                            <input id="name" type="text" class="input @if($errors->has('name')) is-danger @endif" name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                        @if ($errors->has('name'))
                            <p class="help is-danger">
                                <small>{{ $errors->first('name') }}</small>
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="email" class="label">Email</label>
                        <div class="control">
                            <input id="email" type="email" class="input @if($errors->has('name')) is-danger @endif" name="email" value="{{ old('email') }}" required>
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">
                                <small>{{ $errors->first('email') }}</small>
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="psn_name" class="label">PSN Name</label>
                        <div class="control">
                            <input id="psn_name" type="text" class="input" name="psn_name" value="{{ old('psn_name') }}">
                        </div>
                        <p class="help">
                            <small>Optional, we encrypt this data, just in case</small>
                        </p>
                    </div>

                    <div class="field">
                        <label for="password" class="label">Password</label>
                        <div class="control">
                            <input id="password" type="password" class="input @if($errors->has('password')) is-danger @endif" name="password">
                        </div>
                        @if ($errors->has('password'))
                            <p class="help is-danger">
                                <small>{{ $errors->first('password') }}</small>
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="password-confirm" class="label">Confirm Password</label>
                        <div class="control">
                            <input id="password-confirm" type="password" class="input" name="password_confirmation" required>
                        </div>
                    </div>                   

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>            
</div>
@endsection
