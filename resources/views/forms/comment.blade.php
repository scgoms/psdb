@if(Auth::guest())
	You must be registered and logged in to create a comment
@endif
{{ csrf_field() }}
<div class="field">
	<div class="control">
		<textarea name="body" id="comment-form" @if(Auth::guest()) disabled @endif></textarea>			
	</div>
</div>
<div class="field">
	<div class="control">
		<input type="submit" class="button" @if(Auth::guest()) disabled @endif>
	</div>
</div>
@include('layouts.errors')

@section('postscripts')
	<script src="//cdn.ckeditor.com/4.7.1/basic/ckeditor.js"></script>
	<script>
		CKEDITOR.replace('comment-form');
	</script>
@endsection