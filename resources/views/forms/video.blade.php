@if(Auth::guest())
	You must be registered and logged in to create a comment
@endif
{{ csrf_field() }}
<div class="field has-addons">
	<div class="control">
		<input type="text" name="video" class="input" @if(Auth::guest()) disabled @endif>
	</div>
	<div class="control">
		<input type="submit" class="button" @if(Auth::guest()) disabled @endif>
	</div>
</div>

@include('layouts.errors')
