@extends('layouts.master')

@section('content')
<section>
	<div class="card">
		<header class="card-header">
			<p class="card-header-title">
				{{ $user->name }}s profile
			</p>
		</header>
		<div class="card-content">
			@if(Session::has('success'))
				<div class="notification is-success">
					{!! Session::get('success') !!}
				</div>
			@elseif(Session::has('failure'))
				<div class="notification is-danger">
					{!! Session::get('failure') !!}
				</div>
			@endif
			<form action="/profile/{{ $user->id }}" method="POST">
				{{ csrf_field() }}
				<h1 class="title">Your Information</h1>
				<div class="field">
					<label class="label">Username</label>
					<div class="control">
						<input class="input" name="username" type="text" value="{{ $user->name }}">
					</div>
				</div>
				<div class="field">
					<label class="label">PSN Name</label>
					<div class="control">
						<input class="input" name="psn_name" type="text" placeholder="Text input" value="{{ $user->psn_name }}">
					</div>
				</div>
				<h1 class="title">Update Your Password</h1>
				<div class="field">
					<label class="label">Current Password</label>
					<div class="control">
						<input class="input" name="old_password" type="password" placeholder="Current Password">
					</div>
				</div>
				<div class="field">
					<label class="label">New Password</label>
					<div class="control">
						<input class="input" name="password" type="password" placeholder="New Password">
					</div>
				</div>
				<div class="field">
					<label class="label">Confirm</label>
					<div class="control">
						<input class="input" name="confirm_password" type="password" placeholder="Confirm New Password">
					</div>
				</div>
				<h1 class="title">Privacy</h1>
				<div class="field">
					<div class="control">
						<label class="checkbox">
							<input type="checkbox" name="publish_comments" @if($user->publish_comments) checked @endif>
							Show your Comments
						</label>
					</div>
				</div>
				<div class="field">
					<div class="control">
						<label class="checkbox">
							<input type="checkbox" name="publish_images" @if($user->publish_images) checked @endif>
							Show your Images
						</label>
					</div>
				</div>
				<div class="field">
					<div class="control">
						<label class="checkbox">
							<input type="checkbox" name="publish_games" @if($user->publish_games) checked @endif>
							Show your games
						</label>
					</div>
				</div>

				<div class="field is-grouped">
					<div class="control">
						<button class="button is-primary">
							Update
						</button>
					</div>
					<div class="control">
						<button class="button is-link">
							Cancel
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection