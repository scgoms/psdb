<nav class="level">	
	<div class="level-item has-text-centered">
		<div class="total all-total">
			<h1 class="title">{{ count($group->trophies) }}</h1>
		</div>
	</div>
	<div class="level-item has-text-centered">
		<div class="total plat-total">
			<h1 class="title">{{ count($group->trophies->where('type_id', 1)->all()) }}</h1>
		</div>
	</div>
	<div class="level-item has-text-centered">
		<div class="total gold-total">
			<h1 class="title">{{ count($group->trophies->where('type_id', 2)->all()) }}</h1>
		</div>
	</div>
	<div class="level-item has-text-centered">
		<div class="total silver-total">
			<h1 class="title">{{ count($group->trophies->where('type_id', 3)->all()) }}</h1>
		</div>
	</div>
	<div class="level-item has-text-centered">
		<div class="total bronze-total">
			<h1 class="title">{{ count($group->trophies->where('type_id', 4)->all()) }}</h1>
		</div>
	</div>

</nav>