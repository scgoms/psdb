@extends('layouts.master')

@section('content')
	<section>
		<div class="card">
			<header class="card-header">
				<nav class="breadcrumb has-bullet-separator" aria-label="breadcrumbs">
				  <ul>
				    <li><a href="#">Database</a></li>
				    <li><a href="/games/{{ $group->game->id }}">{{ $group->game->title }}</a></li>
				    <li><a href="/groups/{{ $group->id }}">{{ $group->title }}</a></li>
				  </ul>
				</nav>
			</header>
			<div class="card-content">
				<div class="columns">
					<div class="column">
						<figure class="image circle-image" 
						style="background-image: url({{ Storage::disk('s3')->url('images/' . $group->game->id . '/' . $group->id . '/group_logo.PNG') }})">
						</figure>
						<h1 class="title">{{ $group->title }}</h1>
						{{ $group->description }}
					</div>
					<div class="column is-3">
						<div class="box light-box">
							<h1 class="title is-6 has-text-centered">
								Groups
							</h1>
							<ul>
							@foreach($group->game->groups as $a_group)
								<li>
									<a href="/groups/{{ $a_group->id }}">
										@if($group->game->title == $a_group->title)
											Base Game
										@else
											{{ $a_group->title }}
										@endif
									</a>
								</li>
							@endforeach
							</ul>
						</div>
					</div>
					<div class="column is-4">
						<div class="box light-box">
							<h1 class="title is-4 has-text-centered">
								Quick Facts
							</h1>
							<nav class="level">
								<div class="level-item has-text-centered">
									<div>
										<img src="/storage/totalbg.png" class="image is-48x48">
										<p class="heading">Total</p>
										<p class="title">{{ count($group->trophies) }}</p>
									</div>
								</div>
								<div class="level-item has-text-centered">
									<div>
										<img src="/storage/platbg.png" class="image is-48x48">
										<p class="heading">Platinum</p>
										<p class="title">{{ count($group->trophies->where('type_id', 1)->all()) }}</p>
									</div>
								</div>
								<div class="level-item has-text-centered">
									<div>
										<img src="/storage/goldbg.png" class="image is-48x48">
										<p class="heading">Gold</p>
										<p class="title">{{ count($group->trophies->where('type_id', 2)->all()) }}</p>
									</div>
								</div>
								<div class="level-item has-text-centered">
									<div>
										<img src="/storage/silverbg.png" class="image is-48x48">
										<p class="heading">Silver</p>
										<p class="title">{{ count($group->trophies->where('type_id', 3)->all()) }}</p>
									</div>
								</div>
								<div class="level-item has-text-centered">
									<div>
										<img src="/storage/bronzebg.png" class="image is-48x48">
										<p class="heading">Bronze</p>
										<p class="title">{{ count($group->trophies->where('type_id', 4)->all()) }}</p>
									</div>
								</div>
							</nav>
							<h1 class="title is-6 has-text-centered" style="margin-bottom: 0.3em; margin-top: 0.7em">
								Images
							</h1>
							@if(count($group->images))
							<image-gallery>
								@foreach($group->images as $image)
								<image-item path="{{ Storage::disk('s3')->url($image->src) }}"
								@if($group->images->first()->id == $image->id)
								:active=true
								@endif >
								</image-item>
								@endforeach
							</image-gallery>
							@else
								<div class="has-text-centered">
									There doesn't appear to be any images submitted yet! <a href="#image">Be the first!</a>
								</div>
							@endif							
							<h1 class="title is-6 has-text-centered" style="margin-bottom: 0.3em; margin-top: 0.7em">
								Videos
							</h1>
							
							@if(count($group->videos))
							<video-gallery>
								@foreach($group->videos as $image)
								<video-item id="{{ $video->id }}"
								@if($group->images->first()->id == $image->id)
								:active=true
								@endif>
								</video-item>
								@endforeach
							</video-gallery>
							@else
								<div class="has-text-centered">
									There doesn't appear to be any videos submitted yet! <a href="#videos">Be the first!</a>
								</div>
							@endif	
						</div>
					</div>
					
				</div>
			</div>
			<tabs>
				<tab name="Trophies" selected="true">
					<table class="table is-fullwidth is-narrow">
						<thead>
							<tr>
								<td>Type</td>
								<td>Trophy</td>
								<td>Rarity</td>
								<td>Hidden</td>
							</tr>
						</thead>
						<tbody>
							@foreach($group->trophies as $trophy)
							<tr class="trophy-row 
								@if(Auth::check())
									@if(Auth::user()->trophies->contains($trophy->id)) 
									 has-trophy 
									@endif
								@endif">
								<td>
									<div class="psdb-icon 
									@if($trophy->type->id == 1)
										plat-icon
									@elseif($trophy->type->id == 2)
										gold-icon
									@elseif($trophy->type->id == 3)
										silver-icon
									@elseif($trophy->type->id == 4)
										bronze-icon
									@endif
									">										
									</div>
								</td>
								<td>
									<div class="psdb-icon" 
										style="
										margin-right:0.3em;
										float:left;
										background-size: 100% 100%; 
										background-image: url({{ Storage::disk('s3')->url('images/' . $trophy->group->game->id . '/' . $trophy->group->id . '/' . $trophy->id . '/trophy_logo.PNG') }});">
									</div>
									<span style="display:block">
										<a href="/trophies/{{ $trophy->id }}">{{ $trophy->title }}</a>
									</span>
									<small style="display:block">
										{{ $trophy->description }}
									</small>
								</td>
								<td>
									{{ $trophy->rarity }}%
								</td>
								<td>
									@if($trophy->hidden)
										Yes
									@else
										No
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</tab>
				<tab name="Comments">
					@if(count($group->comments))
						@foreach($group->comments as $comment)
							@include('comments.show')
						@endforeach
					@else
					<div class="card-content has-text-centered">
						There doesn't appear to be anything here!<br/>
						@if(Auth::guest())
							<a href="/login">Sign in</a> or <a href="/register">register</a> to leave a comment.
						@else
							<a href="#">Leave a comment</a>
						@endif
					</div>
					@endif
				</tab>
			</tabs>			
		</div>
	</section>
	<section>
		<div class="card">
			<header class="card-header">
				<p class="card-header-title">
					Contribute
				</p>
			</header>
			<tabs>
				<tab name="Comment" selected="true">
					<div class="card-content">
						<form action="/groups/{{ $group->id }}/comment" method="POST">
						@include('forms.comment')
						</form>
					</div>
				</tab>
				<tab name="Submit your Screenshot">
					<div class="card-content">
						<form action="/groups/{{ $group->id }}/image" type="file" method="post" enctype="multipart/form-data">
						@include('forms.image')
						</form>						
					</div>
				</tab>
				<tab name="Suggest a Video">
					<div class="card-content">
						<form action="/groups/{{ $group->id }}/video" method="POST">
						@include('forms.video')
						</form>
					</div>
				</tab>
			</tabs>
		</div>
	</section>			
@endsection