@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="columns">
			<div class="column is-three-quarters">
				@foreach($posts as $post)
					@include('posts.post')
				@endforeach
				<nav>
					{{ $posts->appends([
					'month'=>request('month'), 
					'year'=>request('year'), 
					'tag'=>request('tag')])
					->links() }}
				</nav>
			</div>
			<div class="column">
				@include('layouts.sidebar')
			</div>
		</div>
	</div>	
@endsection