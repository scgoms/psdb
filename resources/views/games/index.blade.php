@extends('layouts.master')

@section('content')
	<section>
		<div class="card">
			<table class="table is-fullwidth">
				<thead>
					<tr>
						<td>Game</td>
					</tr>
				</thead>
				<tbody>
					@foreach($games as $game)
					<tr>
						<td><a href="/games/{{ $game->id }}">{{ $game->title }}</a></td>
					</tr>
					@endforeach
				</tbody>
				<thead>
					<tr>
						<td>Game</td>
					</tr>
				</thead>
			</table>
			<div class="card-content">
				{{ $games->links() }}	
			</div>
		</div>
	</section>
@endsection