<div class="card module">
    <div class="card-content">
        <div class="level is-header">
            <h4 class="title is-4">
                Archives
            </h4>
        </div>
        
        @foreach($archives as $archive)
        <a href="/blog?month={{ $archive->month }}&year={{ $archive->year }}">
            {{ $archive->month }} {{ $archive->year}}
        </a>
        @endforeach
    </div>
</div>
<div class="card module">
    <div class="card-content">
        <div class="level is-header">
            <h4 class="title is-4">
                Categories
            </h4>
        </div>        
        @foreach($tags as $tag)
            <a href="/blog?tag={{ $tag->id }}">
                {{ $tag->name }} ({{ count($tag->posts) }})
            </a>
        @endforeach
    </div>
</div>
@if(count($links))
<div class="card module">
    <div class="card-content">
        <div class="level is-header">
            <h4 class="title is-4">
                Elsewhere
            </h4>
        </div>
        
        @foreach($links as $link)
            <a href="{{ $link->link }}">
                {{ $link->name }}
            </a>
        @endforeach
    </div>
</div>
@endif