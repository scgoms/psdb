<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://use.fontawesome.com/43891b36c1.js"></script>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
          (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-8633129317036083",
            enable_page_level_ads: true
          });
        </script>
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">   
            <div class="container">
                @include('layouts.navbar')
                @yield('content')                
                <footer class="footer over">
                    <div class="container">
                        <div class="columns">
                            <div class="column has-text-centered">
                                <h1 class="title is-6">
                                    Links
                                </h1>
                                <small><a href="http://www.psnleaderboard.com/">API provided by PSN Leaderboard</a></small><br>
                                <small><a href="http://thegoms.com">Portfolio</a></small>
                            </div>
                            <div class="column has-text-centered">
                                <h1 class="title is-6">
                                    Contact
                                </h1>
                                <small><a href="">Contact Me</a></small>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        @yield('prescripts')
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('postscripts')
    </body>
</html>